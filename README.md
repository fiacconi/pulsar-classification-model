# pulsar-classification-model

## Introduction

This repository contains an example implementation of an end-to-end data science project, meaning that we started from a dataset, 
we explored the data and experiment to find a good model, and finally we made the model available within a containerised application.

Specifically, we used an astronomical dataset from the High Time Resolution Survey (HTRU) survey south.
The dataset collects observations of pulsar candidates, in particular statistics of the Integrated Profile (IP) and of the DM-SNR curve, annotated by humans whether they are actual pulsar or not (futher information and the dataset are available [here](https://archive.ics.uci.edu/ml/datasets/HTRU2)).
First, we used a Jupyter notebook (`pulsar-classifier/exploratory_notebooks/exploration.ipynb`) to explore the data and to build a target model.
Then, we ported the results of the exploration in a Python code to build the model and serialize it as an `mlflow` MLmodel (`pulsar-classifier/modelling/make_model.py`).
This script is used to build a Docker image to containerise the model in a Flask/gunicorn application (through the `mlfow models serve` command) and serve the model as a REST API.
Then, we wrote a React web application (`webapp-ui/`) as a GUI to input parameters to be passed to the model and to visualise the resulting predictions.
This frontend is also containerised and served through `nginx` server (set up through `webapp-ui/nginx.conf`), playing also the role of a reverse-proxy to redirect calls to the model API.
The whole application is built and runs through Docker Compose as a two-containers application.

## Repository structure

The repository is organized as follows.

```
pulsar-classification-model/
|__ pulsar-classifier/                  # Directory containing the context of the ML classifier
|   |__ data/                           # Data directory
|   |   |__ pulsar_stars.csv            # Main data file to train the model
|   |__ exploratory_notebooks/          # Exploration directory (unsed to build the model image)
|   |   |__ exploration.ipynb           # Jupyter notebook collecting EDA and model prototyping
|   |   |__ requirements.txt            # Additional requirements to run Jupyter notebook
|   |__ modelling/                      # Modelling directory
|   |   |__ make_model.py               # Python script to train the model during image building
|   |__ .dockerignore                   # .dockerignore to select relevant context
|   |__ Dockerfile                      # Dockerfile to build model serving image/container
|   |__ requirements.txt                # Requirements file to build model image/container
|__ webapp-ui/                          # Directory containing the context for the React web app
|   |__ public/                         # Public files
|   |   |__ favicon.ico                 # Favicon
|   |   |__ index.html                  # Application entry point
|   |__ src/                            # React source code
|   |   |__ App.css                     # Application style sheet
|   |   |__ App.js                      # Application source code
|   |   |__ index.css                   # index style sheet
|   |   |__ index.js                    # index source code
|   |   |__ serviceWorker.js            # Service worker source code
|   |__ .dockerignore                   # .dockerignore to select relevant context
|   |__ Dockerfile                      # Dockerfile to build webapp serving image/container
|   |__ nginx.conf                      # NGINX configuration as web server/reverse proxy
|   |__ package.json                    # Npm package.json
|   |__ package-lock.json               # Npm package-lock.json
|__ .gitignore                          # .gitignore for both Python/Node.js files
|__ docker-compose.yml                  # Docker Compose configuration
|__ README.md                           # This file
```

## How to run the application

The deployment of the application is based on Docker Compose to consistently containerise both the model server and the web app (served through NGINX, also used to redirect calls to the model as a reverse proxy).
Therefore, provided Docker installed, deploying the whole application should be just
```bash
cd pulsar-classification-model/
docker-compose up --build
```
Once Docker has built the images and has run the containers, the application is bound to the port `80` of `localhost`, therefore it should be reached through the browser simply by looking at `http://localhost`.