# -*- coding: utf-8 -*-
import logging
import os
import pickle
import shutil
from functools import wraps

import click
import cloudpickle
import mlflow
import mlflow.models
import mlflow.pyfunc
from mlflow.types.schema import Schema, ColSpec, DataType
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, matthews_corrcoef, roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler

# Set up global logger
log_fmt = '%(asctime)s [%(levelname)s] - %(message)s'
logging.basicConfig(level=logging.INFO, format=log_fmt)


def error_catch(f):
    @wraps(f)
    def inner_func(*args, **kwargs):
        try:
            res = f(*args, **kwargs)
        except Exception as e:
            logging.error(f"ERROR {f.__name__} : {str(e)}")
            raise RuntimeError(f"ERROR {f.__name__} : {str(e)}")
        else:
            return res
    return inner_func


class PulsarClassificationModel(mlflow.pyfunc.PythonModel):
    def __init__(self, col_order):
        self.col_order = col_order
        self.model = None

    def load_context(self, context: mlflow.pyfunc.PythonModelContext) -> None:
        with open(context.artifacts['model'], mode='rb') as f:
            self.model = pickle.load(f)

    def predict(self, context: mlflow.pyfunc.PythonModelContext, model_input: pd.DataFrame):
        x = model_input[self.col_order].values.astype(np.float64)
        y_pred = self.model.predict(x)
        p_pred = self.model.predict_proba(x)[:, np.where(self.model.classes_ == 1)[0][0]]
        return {
            'predicted_category': y_pred,
            'predicted_prob': p_pred
        }


@error_catch
def read_data_into_dataframe(data_file: str) -> pd.DataFrame:
    """
    Reads input data in a DataFrame.
    :param data_file: path to input CSV file.
    :return: raw data.
    """
    df = pd.read_csv(data_file).rename({
        'Mean of the integrated profile': 'avg_ip',
        ' Standard deviation of the integrated profile': 'std_ip',
        ' Excess kurtosis of the integrated profile': 'kur_ip',
        ' Skewness of the integrated profile': 'skw_ip',
        ' Mean of the DM-SNR curve': 'avg_dmsnr',
        ' Standard deviation of the DM-SNR curve': 'std_dmsnr',
        ' Excess kurtosis of the DM-SNR curve': 'kur_dmsnr',
        ' Skewness of the DM-SNR curve': 'skw_dmsnr'
    }, axis=1)
    logging.info(f'Read {df.shape[0]} records from file {data_file}')
    return df


@error_catch
def get_train_test_set(df: pd.DataFrame) -> ((np.array, np.array), (np.array, np.array)):
    """
    Splits input data into train and test set.
    :param df: input data.
    :return: tuples of train and test set data, with features and target classes.
    """
    col_order = ['avg_ip', 'std_ip', 'kur_ip', 'skw_ip', 'avg_dmsnr', 'std_dmsnr', 'kur_dmsnr', 'skw_dmsnr']
    x = df[col_order].values
    y = df['target_class'].values
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1, stratify=y, random_state=1988)
    logging.info(f'Data set split in {y_train.shape[0]} ({y_train.shape[0] / y.shape[0] * 100:.2f}%) train '
                 f'and {y_test.shape[0]} ({y_test.shape[0] / y.shape[0] * 100:.2f}%) test set')
    return (x_train, y_train), (x_test, y_test)


@error_catch
def model_training(train_data: (np.array, np.array)) -> Pipeline:
    """
    Builds and trains a model pipeline.
    :param train_data: training features and target variable.
    :return: trained model.
    """
    x, y = train_data
    model = Pipeline([
        ('scaler', MinMaxScaler()),
        ('classifier', RandomForestClassifier(
            n_estimators=100,
            max_depth=4,
            class_weight='balanced_subsample',
            random_state=1988
        ))
    ])
    logging.info(f'Training model {model}')
    return model.fit(x, y)


@error_catch
def model_evaluation(model, test_data: (np.array, np.array), scoring, out_prob: bool = False) -> float:
    """
    Evaluates model performances on test data according to a metric.
    :param model: model instance to be evaluated.
    :param test_data: test feature and target variables.
    :param scoring: scoring function (from sklearn.metrics.*_score)
    :param out_prob: whether or not output probability of class 1 instead of predicted class.
    :return: evaluated metric.
    """
    x, y = test_data
    y_pred = model.predict(x) if not out_prob else model.predict_proba(x)[:, np.where(model.classes_ == 1)[0][0]]
    return scoring(y, y_pred)


@error_catch
def model_export(model, model_repo: str) -> None:
    """
    Exports the final model as an mlflow.pyfunc custom model.
    :param model: model instance to be exported.
    :param model_repo: path to the mlflow MLmodel directory where the model should be exported.
    """
    logging.info('Exporting model in mlflow.pyfunc flavour')

    # Clean up model directory if already present
    if os.path.exists(model_repo):
        shutil.rmtree(model_repo)

    # Persist the model
    with open('./model.pkl', mode='wb') as f:
        cloudpickle.dump(model, f)

    # Create model schema
    col_order = ['avg_ip', 'std_ip', 'kur_ip', 'skw_ip', 'avg_dmsnr', 'std_dmsnr', 'kur_dmsnr', 'skw_dmsnr']
    model_signature = mlflow.models.signature.ModelSignature(
        inputs=Schema([ColSpec(DataType.double, name) for name in col_order]),
        outputs=Schema([ColSpec(DataType.long, 'predicted_category'), ColSpec(DataType.double, 'predicted_prob')])
    )

    # Store model
    mlflow.pyfunc.save_model(
        path=model_repo,
        python_model=PulsarClassificationModel(col_order),
        artifacts={
            'model': './model.pkl'
        },
        signature=model_signature
    )

    # Finally, remove local pickle of the sklearn model
    if os.path.exists('./model.pkl'):
        os.remove('./model.pkl')

    return


@click.command()
@click.option('-d', '--data', 'datapath', help='Path to data in CSV file', required=True, type=click.Path(exists=True))
@click.option('-m', '--model', 'modelpath', help='Path to store mlflow MLmodel', required=True, type=click.Path())
def main(datapath, modelpath):
    # Read in data in a pandas.DataFrame
    data = read_data_into_dataframe(datapath)

    # Create train and test set
    data_train, data_test = get_train_test_set(data)

    # Build and train the model
    model = model_training(data_train)

    # Evaluate the model
    test_metrics = {
        'accuracy': model_evaluation(model, data_test, accuracy_score),
        'precision': model_evaluation(model, data_test, precision_score),
        'recall': model_evaluation(model, data_test, recall_score),
        'f1': model_evaluation(model, data_test, f1_score),
        'matthews_corr': model_evaluation(model, data_test, matthews_corrcoef),
        'auc': model_evaluation(model, data_test, roc_auc_score, out_prob=True)
    }
    for k, v in test_metrics.items():
        logging.info(f'Model evaluation on test set {k.upper():20s}= {v:.6f}')

    # Persist the model with mlflow
    model_export(model, modelpath)
    return


if __name__ == "__main__":
    main()
