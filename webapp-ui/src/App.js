import React from 'react';
import { Row, Col, Container, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import 'process';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';


function App() {

  const [inputFeatures, setInputFeatures] = React.useState(undefined);

  React.useEffect(() => {
    
    const setInference = (result) => {
      document.getElementById('prediction_class').innerText = (result.predicted_category === 1) ? 'Pulsar' : 'Not a pulsar';
      document.getElementById('prediction_prob').innerText = `${Math.floor(result.predicted_prob * 1000) / 10} %`;
    };

    const arrangeFeatures = (feat) => {
      return {
        columns: Object.keys(feat),
        data: [Object.keys(feat).map(k => feat[k])]
      }
    };
  
    const computePrediction = (features) => {
      
      if (!features) return;

      // FETCH TO GET PREDICTION
      if (process.env.NODE_ENV === 'develop' || process.env.NODE_ENV === 'development') {
        setInference({
          predicted_category: 1,
          predicted_prob: 0.15
        });
      } else {
        const payload = arrangeFeatures(features);
        
        fetch(`/api/invocations`, {
          method: 'POST',
          headers: {
            'Conten-Type': 'application/json'
          },
          body: JSON.stringify(payload)
        }).then(res => res.json()).then((data) => {
          const { predicted_category, predicted_prob } = data;
          setInference({
            predicted_category: predicted_category[0],
            predicted_prob: predicted_prob[0]
          });
        }).catch((err) => console.log(err));
      }
    };

    computePrediction(inputFeatures);
  }, [inputFeatures]);

  const notNumberFilter = (s) => s.replace(/[^0-9.]/, '');

  const getFeatures = () => {
    return {
      avg_ip: parseFloat(document.getElementById('avg_ip').value) || 0.0,
      std_ip: parseFloat(document.getElementById('std_ip').value) || 0.0,
      skw_ip: parseFloat(document.getElementById('skw_ip').value) || 0.0,
      kur_ip: parseFloat(document.getElementById('kur_ip').value) || 0.0,
      avg_dmsnr: parseFloat(document.getElementById('avg_dmsnr').value) || 0.0,
      std_dmsnr: parseFloat(document.getElementById('std_dmsnr').value) || 0.0,
      skw_dmsnr: parseFloat(document.getElementById('skw_dmsnr').value) || 0.0,
      kur_dmsnr: parseFloat(document.getElementById('kur_dmsnr').value) || 0.0,
    };
  };

  return (
    <div className="App">
      <div className="App-title">
        Pulsar prediction model
        </div>
      <Container>
        <Row>
          <Col md="6">
            <div className="App-feature-group">
              <div className="App-feature-group-title">
                Integrated Profile
                </div>
              <Form>
                <FormGroup row className="App-input-feature">
                  <Label for="avg_ip" sm={6}>Average</Label>
                  <Col sm={6}>
                    <Input type="text" name="avg_ip" id="avg_ip" placeholder="Example: 1.24"
                      onChange={(e) => { e.target.value = notNumberFilter(e.target.value); }} />
                  </Col>
                </FormGroup>
                <FormGroup row className="App-input-feature">
                  <Label for="std_ip" sm={6}>Standard Deviation</Label>
                  <Col sm={6}>
                    <Input type="text" name="std_ip" id="std_ip" placeholder="Example: 1.24"
                      onChange={(e) => { e.target.value = notNumberFilter(e.target.value); }} />
                  </Col>
                </FormGroup>
                <FormGroup row className="App-input-feature">
                  <Label for="skw_ip" sm={6}>Skewness</Label>
                  <Col sm={6}>
                    <Input type="text" name="skw_ip" id="skw_ip" placeholder="Example: 1.24"
                      onChange={(e) => { e.target.value = notNumberFilter(e.target.value); }} />
                  </Col>
                </FormGroup>
                <FormGroup row className="App-input-feature">
                  <Label for="kur_ip" sm={6}>Excess kurtosis</Label>
                  <Col sm={6}>
                    <Input type="text" name="kur_ip" id="kur_ip" placeholder="Example: 1.24"
                      onChange={(e) => { e.target.value = notNumberFilter(e.target.value); }} />
                  </Col>
                </FormGroup>
              </Form>
            </div>
          </Col>
          <Col md="6">
            <div className="App-feature-group">
              <div className="App-feature-group-title">
                DM-SNR curve
                </div>
              <Form>
                <FormGroup row className="App-input-feature">
                  <Label for="avg_dmsnr" sm={6}>Average</Label>
                  <Col sm={6}>
                    <Input type="text" name="avg_dmsnr" id="avg_dmsnr" placeholder="Example: 1.24"
                      onChange={(e) => { e.target.value = notNumberFilter(e.target.value); }} />
                  </Col>
                </FormGroup>
                <FormGroup row className="App-input-feature">
                  <Label for="std_dmsnr" sm={6}>Standard Deviation</Label>
                  <Col sm={6}>
                    <Input type="text" name="std_dmsnr" id="std_dmsnr" placeholder="Example: 1.24"
                      onChange={(e) => { e.target.value = notNumberFilter(e.target.value); }} />
                  </Col>
                </FormGroup>
                <FormGroup row className="App-input-feature">
                  <Label for="skw_dmsnr" sm={6}>Skewness</Label>
                  <Col sm={6}>
                    <Input type="text" name="skw_dmsnr" id="skw_dmsnr" placeholder="Example: 1.24"
                      onChange={(e) => { e.target.value = notNumberFilter(e.target.value); }} />
                  </Col>
                </FormGroup>
                <FormGroup row className="App-input-feature">
                  <Label for="kur_dmsnr" sm={6}>Excess kurtosis</Label>
                  <Col sm={6}>
                    <Input type="text" name="kur_dmsnr" id="kur_dmsnr" placeholder="Example: 1.24"
                      onChange={(e) => { e.target.value = notNumberFilter(e.target.value); }} />
                  </Col>
                </FormGroup>
              </Form>
            </div>
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <div className="App-submit">
              <Button onClick={() => setInputFeatures(getFeatures())}>Compute Prediction</Button>
            </div>
          </Col>
        </Row>
        {
          (inputFeatures) ? 
          <Row>
            <Col md="6">
              <div className='App-prediction-block'>
                Predicted class: <span id='prediction_class'></span>
              </div>
            </Col>
            <Col md="6">
              <div className='App-prediction-block'>
                Inference probability: <span id='prediction_prob'></span>
              </div>
            </Col>
          </Row> : null
        }
      </Container>
    </div>
  );

}

export default App;
